/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo.tipoDocente;

import implementacion.tipoDocente.OcacionalImpl;
import modelo.Docente;

/**
 *
 * @author andre
 */
public class Ocacional extends Docente implements OcacionalImpl{
    private String ultimoTitulo;
    private int numeroMeses;
    private int salario;

    public Ocacional(String ultimoTitulo, int numeroMeses, int salario, String areaFormacion, String tituloProfesional, String unidadAcademica, int idPersona, String nombrePersona, String apellidoPersona, String fechaNacimiento, String ciudadNacimiento, String genero, int estrato) {
        super(areaFormacion, tituloProfesional, unidadAcademica, idPersona, nombrePersona, apellidoPersona, fechaNacimiento, ciudadNacimiento, genero, estrato);
        this.ultimoTitulo = ultimoTitulo;
        this.numeroMeses = numeroMeses;
        this.salario = salario;
    }

  

    @Override
    public String toString() {
        return "Ocacional{" + "ultimoTitulo=" + ultimoTitulo + ", numeroMeses=" + numeroMeses + ", salario=" + salario + '}';
    }

    public String getUltimoTitulo() {
        return ultimoTitulo;
    }

    public void setUltimoTitulo(String ultimoTitulo) {
        this.ultimoTitulo = ultimoTitulo;
    }

    public int getNumeroMeses() {
        return numeroMeses;
    }

    public void setNumeroMeses(int numeroMeses) {
        this.numeroMeses = numeroMeses;
    }

    public int getSalario() {
        return salario;
    }

    public void setSalario(int salario) {
        this.salario = salario;
    }

    @Override
    public double calculaSueldo() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double liquidarOC() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mostrarLiqOC() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
