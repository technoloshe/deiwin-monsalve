/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo.tipoDocente;

import implementacion.tipoDocente.TiempoCompletoImpl;
import modelo.Docente;

/**
 *
 * @author andre
 */
public class TiempoCompleto extends Docente implements TiempoCompletoImpl{
    private String categoria;
    private int puntos;
    private int salario;

    @Override
    public String toString() {
        return "TiempoCompleto{" + "categoria=" + categoria + ", puntos=" + puntos + ", salario=" + salario + '}';
    }

    public TiempoCompleto(String categoria, int puntos, int salario, String areaFormacion, String tituloProfesional, String unidadAcademica, int idPersona, String nombrePersona, String apellidoPersona, String fechaNacimiento, String ciudadNacimiento, String genero, int estrato) {
        super(areaFormacion, tituloProfesional, unidadAcademica, idPersona, nombrePersona, apellidoPersona, fechaNacimiento, ciudadNacimiento, genero, estrato);
        this.categoria = categoria;
        this.puntos = puntos;
        this.salario = salario;
    }

   

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public int getPuntos() {
        return puntos;
    }

    public void setPuntos(int puntos) {
        this.puntos = puntos;
    }

    public int getSalario() {
        return salario;
    }

    public void setSalario(int salario) {
        this.salario = salario;
    }

    @Override
    public double calculaSueldo() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double liquidarTC() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mostrarLiqTC() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
