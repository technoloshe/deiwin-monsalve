/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo.tipoDocente;

import implementacion.tipoDocente.HoraCatedraImpl;
import modelo.Docente;

/**
 *
 * @author andre
 */
public class HoraCatedra extends Docente implements HoraCatedraImpl{
   private String ultimoTitulo;
   private int numHoras;
   private int valorContrato;
   private int salario;

    @Override
    public String toString() {
        return "HoraCatedra{" + "ultimoTitulo=" + ultimoTitulo + ", numHoras=" + numHoras + ", valorContrato=" + valorContrato + ", salario=" + salario + '}';
    }

    public HoraCatedra(String ultimoTitulo, int numHoras, int valorContrato, int salario, String areaFormacion, String tituloProfesional, String unidadAcademica, int idPersona, String nombrePersona, String apellidoPersona, String fechaNacimiento, String ciudadNacimiento, String genero, int estrato) {
        super(areaFormacion, tituloProfesional, unidadAcademica, idPersona, nombrePersona, apellidoPersona, fechaNacimiento, ciudadNacimiento, genero, estrato);
        this.ultimoTitulo = ultimoTitulo;
        this.numHoras = numHoras;
        this.valorContrato = valorContrato;
        this.salario = salario;
    }

   

    public String getUltimoTitulo() {
        return ultimoTitulo;
    }

    public void setUltimoTitulo(String ultimoTitulo) {
        this.ultimoTitulo = ultimoTitulo;
    }

    public int getNumHoras() {
        return numHoras;
    }

    public void setNumHoras(int numHoras) {
        this.numHoras = numHoras;
    }

    public int getValorContrato() {
        return valorContrato;
    }

    public void setValorContrato(int valorContrato) {
        this.valorContrato = valorContrato;
    }

    public int getSalario() {
        return salario;
    }

    public void setSalario(int salario) {
        this.salario = salario;
    }

    @Override
    public double calculaSueldo() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double liquidarHC() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mostrarLiqHC() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
   
}
