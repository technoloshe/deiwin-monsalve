/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo.administrativo;

import implementacion.administrativo.OPSImpl;
import java.util.Date;
import modelo.Administrativo;


/**
 *
 * @author andre
 */
public class OPS extends Administrativo implements OPSImpl{
    private Date fechaVinculacion;
    private int numMeses;
    private int valorContrato;
    private int salario;

    @Override
    public String toString() {
        return "OPS{" + "fechaVinculacion=" + fechaVinculacion + ", numMeses=" + numMeses + ", valorContrato=" + valorContrato + ", salario=" + salario + '}';
    }

    public OPS(Date fechaVinculacion, int numMeses, int valorContrato, int salario, String dependencia, String titulo, int idPersona, String nombrePersona, String apellidoPersona, String fechaNacimiento, String ciudadNacimiento, String genero, int estrato) {
        super(dependencia, titulo, idPersona, nombrePersona, apellidoPersona, fechaNacimiento, ciudadNacimiento, genero, estrato);
        this.fechaVinculacion = fechaVinculacion;
        this.numMeses = numMeses;
        this.valorContrato = valorContrato;
        this.salario = salario;
    }

   
    public Date getFechaVinculacion() {
        return fechaVinculacion;
    }

    public void setFechaVinculacion(Date fechaVinculacion) {
        this.fechaVinculacion = fechaVinculacion;
    }

    public int getNumMeses() {
        return numMeses;
    }

    public void setNumMeses(int numMeses) {
        this.numMeses = numMeses;
    }

    public int getValorContrato() {
        return valorContrato;
    }

    public void setValorContrato(int valorContrato) {
        this.valorContrato = valorContrato;
    }

    public int getSalario() {
        return salario;
    }

    public void setSalario(int salario) {
        this.salario = salario;
    }

    @Override
    public double liquidarValorContrato() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mostrarLiqOPS() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
