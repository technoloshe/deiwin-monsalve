/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo.administrativo;

import implementacion.administrativo.PlantaImpl;
import java.util.Date;
import modelo.Administrativo;


/**
 *
 * @author andre
 */
public class Planta extends Administrativo implements PlantaImpl {
    private Date fechaVinculacion;

    @Override
    public String toString() {
        return "Planta{" + "fechaVinculacion=" + fechaVinculacion + '}';
    }

 

    public Planta(Date fechaVinculacion, String dependencia, String titulo, int idPersona, String nombrePersona, String apellidoPersona, String fechaNacimiento, String ciudadNacimiento, String genero, int estrato) {
        super(dependencia, titulo, idPersona, nombrePersona, apellidoPersona, fechaNacimiento, ciudadNacimiento, genero, estrato);
        this.fechaVinculacion = fechaVinculacion;
    }
    
    public Date getFechaVinculacion() {
        return fechaVinculacion;
    }

    public void setFechaVinculacion(Date fechaVinculacion) {
        this.fechaVinculacion = fechaVinculacion;
    }

    @Override
    public void mostrarDatosAdminPlanta() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
  
}
