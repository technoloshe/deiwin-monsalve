/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo.administrativo.planta;

import implementacion.administrativo.planta.TecnicoImpl;
import java.util.Date;
import modelo.administrativo.Planta;

/**
 *
 * @author andre
 */
public class Tecnico extends Planta implements TecnicoImpl{
   private int nivel;
   private int salario; 

    @Override
    public String toString() {
        return "Tecnico{" + "nivel=" + nivel + ", salario=" + salario + '}';
    }

    public Tecnico(int nivel, int salario, Date fechaVinculacion, String dependencia, String titulo, int idPersona, String nombrePersona, String apellidoPersona, String fechaNacimiento, String ciudadNacimiento, String genero, int estrato) {
        super(fechaVinculacion, dependencia, titulo, idPersona, nombrePersona, apellidoPersona, fechaNacimiento, ciudadNacimiento, genero, estrato);
        this.nivel = nivel;
        this.salario = salario;
    }


   
    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public int getSalario() {
        return salario;
    }

    public void setSalario(int salario) {
        this.salario = salario;
    }

    @Override
    public double calcularSueldo() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double liquidarTec() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mostrarLiqTec() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
   
}
