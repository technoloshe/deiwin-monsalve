/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import implementacion.AdministrativoImpl;

/**
 *
 * @author andre
 */
public class Administrativo extends Persona implements AdministrativoImpl{
    private String dependencia;
    private String titulo;

    public Administrativo(String dependencia, String titulo, int idPersona, String nombrePersona, String apellidoPersona, String fechaNacimiento, String ciudadNacimiento, String genero, int estrato) {
        super(idPersona, nombrePersona, apellidoPersona, fechaNacimiento, ciudadNacimiento, genero, estrato);
        this.dependencia = dependencia;
        this.titulo = titulo;
    }

    public String getDependencia() {
        return dependencia;
    }

    public void setDependencia(String dependencia) {
        this.dependencia = dependencia;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    @Override
    public void mostrarDatosAdmini() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }    
}
