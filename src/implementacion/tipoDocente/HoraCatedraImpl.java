/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package implementacion.tipoDocente;

/**
 *
 * @author andre
 */
public interface HoraCatedraImpl {
    public double calculaSueldo();
    public double liquidarHC();
    public void mostrarLiqHC();
}
