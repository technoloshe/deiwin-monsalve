/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package implementacion;

/**
 *
 * @author andre
 */
public interface PersonaImpl {
     public void mostrarDG();
    public double calculaEPS( );
    public double calculaPension();
    public double calculaARL();
    public double calculaSENA();
    public double calculaCajas();
    public double calculaICBF();
    public double calculaAuxilio();
}
